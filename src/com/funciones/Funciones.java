package com.funciones;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.modelo.JdbcConexion;

public class Funciones {
	private Integer _error = 0;
	private Connection myconexion2 = new JdbcConexion().init();

	public Funciones() {

		_error = 0;
	}

	public String getCoutQuestions(String id) {

		if (myconexion2 != null) {
			try {
				String sql = "select count(id) as c from question where \"idQuestionnaire\"="
						+ id + " ";
				Statement st = myconexion2.createStatement();
				ResultSet rs = st.executeQuery(sql);
				if (rs.next()) {
					return rs.getString("c");
				} else {
					return "0";
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			System.out.println("Conexion insatisfactoria a la base de datos");
			return "Error - 404";
		}
		return "0";
	}

	/*
	 * public String getSimpleDataList(String table, String label, String
	 * value,String term, String paramOrder){
	 * 
	 * if (myconexion2 != null) { try { String sql = "select "+ label
	 * +", "+value
	 * +" from "+table+" where title like '%"+term+"%' order by "+paramOrder;
	 * Statement st = myconexion2.createStatement(); ResultSet rs =
	 * st.executeQuery(sql); String jsonString = "[";
	 * 
	 * if (rs.next()) { jsonString = jsonString +
	 * "{\"label\": \""+rs.getString(value
	 * )+"|"+rs.getString(label)+"\", \"value\": \""+rs.getString(label)+"\" }";
	 * while (rs.next()) { jsonString = jsonString +
	 * ",{\"label\": \""+rs.getString
	 * (value)+"|"+rs.getString(label)+"\", \"value\": \""
	 * +rs.getString(label)+"\" }"; } } else{ return
	 * "No se encontraron Registros"; }
	 * 
	 * jsonString = jsonString + "]"; return jsonString;
	 * 
	 * } catch (Exception e) { System.out.println(e.getMessage());
	 * e.printStackTrace(); }
	 * 
	 * } else {
	 * System.out.println("Conexion insatisfactoria a la base de datos"); return
	 * "Error - 404";//Numero cualquiera, faltahacer un control de estos errores
	 * }
	 * 
	 * return ""; }
	 */

	public List<String> getSimpleDataList(String table, String label,
			String value, String term, String paramOrder) {

		if (myconexion2 != null) {
			try {
				String sql = "select " + label + ", " + value + " from "
						+ table + " where " + label + " ilike '%" + term
						+ "%' order by " + paramOrder;
				Statement st = myconexion2.createStatement();
				ResultSet rs = st.executeQuery(sql);

				List<String> results = new ArrayList<String>();
				// results.add(query + i);

				if (rs.next()) {
					results.add(rs.getString(label));

					while (rs.next()) {
						results.add(rs.getString(label));
					}
				} else {
					results.add("No se encontraron registros");
				}

				return results;

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			System.out.println("Conexion insatisfactoria a la base de datos");
			return null;// Numero cualquiera, faltahacer un control de
						// estos errores
		}

		return null;
	}

	public List<String> getCoursesList(String term) {
		return this.getSimpleDataList("course", "description", "id", term,
				"description");
	}

	public List<String> getQuestionnaireList(String term) {
		return this.getSimpleDataList("Questionnaire", "title", "id", term,
				"title");

	}

}
