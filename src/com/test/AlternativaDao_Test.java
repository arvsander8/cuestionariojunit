package com.test;

import static org.junit.Assert.*;

import com.modelo.Alternative;
import com.modelo.Question;
import com.dao.AlternativaDAO;

import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

public class AlternativaDao_Test {

	private AlternativaDAO alternativaDao;
	private Question pregunta;
	private List<Alternative> lista_alternativas;
	private final Integer Nalternativas = 4;
	
	@Test
	public void DaoAlternativa() throws Exception{
		
		alternativaDao = new AlternativaDAO();
		pregunta = new Question();
		pregunta.setId(1);
		
		try{
			lista_alternativas = alternativaDao.Alternativas(pregunta);
		}catch(Exception e){
			throw e;
		}
		
		System.out.println("Acceso a datos clase Alternativas... ");
		assertTrue(lista_alternativas.size() == Nalternativas);
	}
	
}
