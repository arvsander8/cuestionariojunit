package com.test;

import static org.junit.Assert.*;

import com.modelo.Users;
import com.dao.UsuarioDAO;

import org.junit.Test;

public class UsuarioDao_Test {
	
	private UsuarioDAO usuDAO;
	private Users usuTest;
	private Users usuResult;
	private final Integer IDusuario = 1;
	
	@Test
	public void DaoUsuario() throws Exception{
		
		usuTest = new Users();
		usuResult = new Users();
		usuTest.setNick("arvsander8");
		usuTest.setPassword("123456");
		usuDAO = new UsuarioDAO();
		try{
		usuResult = usuDAO.verifiacarDatos(usuTest);
		}catch(Exception e){
			throw e;
		}
		System.out.println("Acceso a datos de un usuario ... ");
	    assertTrue(usuResult.getId() == IDusuario);
	}

	
}
