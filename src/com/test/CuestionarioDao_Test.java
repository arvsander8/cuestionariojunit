package com.test;

import static org.junit.Assert.*;

import com.modelo.Questionnaire;
import com.dao.CuestionarioDAO;

import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

public class CuestionarioDao_Test {

	private Questionnaire cuestionario;
	private CuestionarioDAO cuestionarioDao;
	private List<Questionnaire> lista_cuestionarios;
	private final Integer TotalCuestionarios = 13;
	
	@Test
	public void DaoCuestionario() throws Exception{
		
		cuestionarioDao = new CuestionarioDAO();
		try{
			lista_cuestionarios = cuestionarioDao.Listar();
		}catch(Exception e){
			throw e;
		}
		
		System.out.println("Acceso a datos clase Cuestionarios ... ");
	    assertTrue(lista_cuestionarios.size() == TotalCuestionarios);
	}
}
