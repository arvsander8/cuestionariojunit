package com.test;

import static org.junit.Assert.*;

import com.modelo.Question;
import com.modelo.Questionnaire;
import com.dao.PreguntaDAO;

import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

public class PreguntaDao_Test {

	private Question pregunta;
	private Questionnaire cuestionario;
	private PreguntaDAO preguntaDao;
	
	private final Integer TotalPreguntas = 10;
	
	@Test
	public void DaoPregunta() throws Exception{
		List<Question> lista_preguntas;
		cuestionario = new Questionnaire();
		cuestionario.setId(1);
		preguntaDao = new PreguntaDAO();
		try{
			lista_preguntas = preguntaDao.Preguntas(cuestionario);
		}catch(Exception e){
			throw e;
		}
		System.out.println("Acceso a datos clase Pregunta");
		assertTrue(lista_preguntas.size() == TotalPreguntas);
	}
	
}

