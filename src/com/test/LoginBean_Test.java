package com.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.beans.LoginBean;
import com.modelo.Users;

public class LoginBean_Test {

	private LoginBean login;
	private Users usuario;
	private String resultado;
	
	@Test
	public void LoginBean() throws Exception{
		
		usuario = new Users();
		resultado = "";
		usuario.setNick("arvsander8");
		usuario.setPassword("123456");
		login = new LoginBean();
		try{
			login.setUsuario(usuario);
			resultado = login.verificarDatos();
		}catch(Exception e){
			throw e;
		}
		System.out.println("Login de un usuario ... ");
	    assertTrue(resultado == "resultado");
	}

	
}
