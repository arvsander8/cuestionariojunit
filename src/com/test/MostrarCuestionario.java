package com.test;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class MostrarCuestionario {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/CuestionarioHibernate/faces/login.xhtml";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testMostrarCuestionarios() throws Exception {
    driver.get(baseUrl + "/CuestionarioHibernate/faces/login.xhtml");
    driver.findElement(By.id("j_idt23:user")).clear();
    driver.findElement(By.id("j_idt23:user")).sendKeys("arv");
    driver.findElement(By.id("j_idt23:user")).clear();
    driver.findElement(By.id("j_idt23:user")).sendKeys("arvsander8");
    driver.findElement(By.id("j_idt23:pass")).clear();
    driver.findElement(By.id("j_idt23:pass")).sendKeys("123456");
    driver.findElement(By.name("j_idt23:j_idt29")).click();
    driver.findElement(By.id("j_idt25:j_idt30")).click();
    driver.findElement(By.name("j_idt25:j_idt31:j_idt32")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
