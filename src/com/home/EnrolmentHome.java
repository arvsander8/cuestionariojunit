package com.home;

// Generated 14/07/2014 08:12:34 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.modelo.Enrolment;

/**
 * Home object for domain model class Enrolment.
 * @see com.modelo.Enrolment
 * @author Hibernate Tools
 */
public class EnrolmentHome {

	private static final Log log = LogFactory.getLog(EnrolmentHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Enrolment transientInstance) {
		log.debug("persisting Enrolment instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Enrolment instance) {
		log.debug("attaching dirty Enrolment instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Enrolment instance) {
		log.debug("attaching clean Enrolment instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Enrolment persistentInstance) {
		log.debug("deleting Enrolment instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Enrolment merge(Enrolment detachedInstance) {
		log.debug("merging Enrolment instance");
		try {
			Enrolment result = (Enrolment) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Enrolment findById(int id) {
		log.debug("getting Enrolment instance with id: " + id);
		try {
			Enrolment instance = (Enrolment) sessionFactory.getCurrentSession()
					.get("com.modelo.Enrolment", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Enrolment instance) {
		log.debug("finding Enrolment instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.modelo.Enrolment")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
